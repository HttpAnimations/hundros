// repo.js
function loadRepos() {
    fetch('/read-file?filePath=Movies/repo.json')
      .then(response => response.text())
      .then(data => {
        const repos = JSON.parse(data).urls;
        const list = document.getElementById('repoList');
        list.innerHTML = '';
        repos.forEach((url, index) => {
          const li = document.createElement('li');
          li.textContent = url;
          const removeBtn = document.createElement('button');
          removeBtn.textContent = 'Remove';
          removeBtn.onclick = () => removeRepo(index);
          li.appendChild(removeBtn);
          list.appendChild(li);
        });
      });
  }
  
  function addRepo() {
    const url = document.getElementById('newRepoUrl').value;
    if (url) {
      fetch('/read-file?filePath=Movies/repo.json')
        .then(response => response.text())
        .then(data => {
          const json = JSON.parse(data);
          json.urls.push(url);
          return fetch('/save-file', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              filePath: 'Movies/repo.json',
              content: JSON.stringify(json, null, 2)
            })
          });
        })
        .then(() => {
          loadRepos();
          alert('Repo added successfully!');
        })
        .catch(err => {
          alert('Failed to add repo: ' + err);
        });
    }
  }
  
  function removeRepo(index) {
    fetch('/read-file?filePath=Movies/repo.json')
      .then(response => response.text())
      .then(data => {
        const json = JSON.parse(data);
        json.urls.splice(index, 1);
        return fetch('/save-file', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            filePath: 'Movies/repo.json',
            content: JSON.stringify(json, null, 2)
          })
        });
      })
      .then(() => {
        loadRepos();
        alert('Repo removed successfully!');
      })
      .catch(err => {
        alert('Failed to remove repo: ' + err);
      });
  }
  
  document.addEventListener('DOMContentLoaded', loadRepos);
  