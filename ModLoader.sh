#!/bin/bash

# Create Mods.html file
echo "<!DOCTYPE html>" > Mods.html
echo "<html lang=\"en\">" >> Mods.html
echo "" >> Mods.html
echo "<head>" >> Mods.html
echo "<!-- Only edit this file from ModLoader and not Mods.html as the file will be overwritten once a mod is compiled. -->" >> Mods.html
echo "    <meta charset=\"UTF-8\">" >> Mods.html
echo "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" >> Mods.html
echo "    <title>Hundros</title>" >> Mods.html
echo "    <link rel=\"stylesheet\" href=\"CSS/Mods.css\">" >> Mods.html
echo "    <script src=\"fetchVersion.js\"></script>" >> Mods.html
echo "    <script src=\"getDataBasedOfSearchBarAndDisplayIt.js\"></script>" >> Mods.html
echo "</head>" >> Mods.html
echo "" >> Mods.html
echo "<body onload=\"fetchVersion()\">" >> Mods.html
echo "    <div class=\"header\">" >> Mods.html
echo "        <a href=\"index.html\">" >> Mods.html
echo "            <img src=\"612icon.png\" alt=\"Logo\">" >> Mods.html
echo "        </a>" >> Mods.html
echo "        <div class=\"header-text\"><a href=\"Repos.html\">Repos</a></div>" >> Mods.html
echo "        <div class=\"header-text\"><a href=\"Settings.html\">Settings</a></div>" >> Mods.html
echo "        <div class=\"header-text\"><a href=\"Versions.html\">Versions</a></div>" >> Mods.html
echo "        <div class=\"header-text\"><a href=\"Mods.html\">Mods</a></div>" >> Mods.html
echo "    </div>" >> Mods.html
echo "    <div class=\"content\">" >> Mods.html
# echo "      <h4>As of 1.1.1 mods are still being worked on so don't expect docs for a few</h4>" >> Mods.html

# Loop through all package.json files in Mods folder and subdirectories
find Mods -name "package.json" | while read -r file; do
    # Extract information from package.json
    name=$(jq -r ".name" "$file")
    main=$(jq -r ".main" "$file")
    dir=$(jq -r ".dir" "$file")
    author=$(jq -r ".author" "$file")
    version=$(jq -r ".version" "$file")
    info=$(jq -r ".info" "$file")
    
    # Create button for each mod
    echo "        <a href=\"$dir/$main\" class=\"mod-button\">$name</a>" >> Mods.html
    echo "        <div class=\"mod-info\">" >> Mods.html
    echo "            <div class=\"mod-author\">Author: $author</div>" >> Mods.html
    echo "            <div class=\"mod-version\">Version: $version</div>" >> Mods.html
    echo "            <div class=\"mod-description\">Info: $info</div>" >> Mods.html
    echo "        </div>" >> Mods.html
done

echo "    </div>" >> Mods.html
echo "    <br>" >> Mods.html
echo "    <div class=\"footer\">" >> Mods.html
echo "        <div id=\"version\">Loading version...</div>" >> Mods.html
echo "        <a href=\"https://gitlab.com/HttpAnimations/hundros\">" >> Mods.html
echo "            <i class=\"fa-brands fa-gitlab\"></i> Gitlab" >> Mods.html
echo "        </a>" >> Mods.html
echo "    </div>" >> Mods.html
echo "</body>" >> Mods.html
echo "" >> Mods.html
echo "</html>" >> Mods.html


echo "Completed restart Hundros for changes to apply. :3"