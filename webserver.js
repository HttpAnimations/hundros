const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();
const port = 3000; // Specify the port you want to use

// Serve static files from the root directory
app.use(express.static(path.join(__dirname)));

// Serve the index.html file
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// Endpoint to read a file
app.get('/read-file', (req, res) => {
  const filePath = req.query.filePath;
  try {
    const content = fs.readFileSync(path.join(__dirname, filePath), 'utf8');
    res.send(content);
  } catch (error) {
    console.error('Failed to read file:', error);
    res.status(500).send('Failed to read file');
  }
});

// Endpoint to save a file
app.post('/save-file', express.json(), (req, res) => {
  const { filePath, content } = req.body;
  try {
    fs.writeFileSync(path.join(__dirname, filePath), content, 'utf8');
    res.send('File saved successfully');
  } catch (error) {
    console.error('Failed to save file:', error);
    res.status(500).send('Failed to save file');
  }
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
