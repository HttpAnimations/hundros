async function fetchJSON(url) {
    try {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Failed to fetch JSON from ' + url, error);
        return null; // Return null in case of failure to handle it gracefully
    }
}

async function loadTorrents() {
    const repoInfo = await fetchJSON('Versions.json'); // Adjust this URL if necessary
    if (!repoInfo || !repoInfo.url) {
        console.error('Invalid repository info');
        document.getElementById('contentArea').innerHTML = '<p>Error fetching repository information. Check console for details.</p>';
        return;
    }

    const torrentsData = await fetchJSON(repoInfo.url); // Fetching data from the URL provided in MRepo.json
    if (!torrentsData || !torrentsData.Torrents) {
        console.error('Invalid torrents data');
        document.getElementById('contentArea').innerHTML = '<p>Error fetching torrents data. Check console for details.</p>';
        return;
    }

    const contentArea = document.getElementById('contentArea');
    contentArea.innerHTML = '<h2>Versions:</h2>'; // Clear initial content or add title

    torrentsData.Torrents.forEach(torrent => {
        const torrentDiv = document.createElement('div');
        torrentDiv.className = 'torrent';
        torrentDiv.innerHTML = `
<h4>${torrent.Name}</h4>
<div class="download-section">
    <p>Download:</p>
    <a href="${torrent.GenericLinux}" class="button">Generic Linux - ZIP</a>
    <a href="${torrent[".Torrent"]}" download="${torrent[".Torrent"].split('/').pop()}" class="button">Linux - DEB</a>
    <a href="${torrent.RPM}" class="button">Linux - RPM</a>
    <a href="${torrent.MagnetUrl}" class="button">macOS - ZIP - > APP</a>
    <a href="${torrent.NTDownload}" class="button">NT - EXE</a>

</div>
`;
        contentArea.appendChild(torrentDiv);
    });
}

loadTorrents();