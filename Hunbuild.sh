#!/bin/bash

# Ask for the name
read -p "Enter the name: " name

# Make a copy of the folder REPLACEME
cp -r REPLACEME "$name"

# Replace REPLACEME with the name in file names
find "$name" -depth -name "*REPLACEME*" | while IFS= read -r file ; do
  newfile="$(echo "$file" | sed "s/REPLACEME/$name/g")"
  mv "$file" "$newfile"
done

# Replace REPLACEME with the name in file contents
grep -rl "REPLACEME" "$name" | while IFS= read -r file ; do
  content=$(<"$file")
  newcontent=$(echo "$content" | sed "s/REPLACEME/$name/g")
  echo "$newcontent" > "$file"
done

echo "Replacement completed."
