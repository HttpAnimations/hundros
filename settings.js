function fetchVersionAndUpdate() {
    fetch('package.json')
        .then(response => response.json())
        .then(data => {
            const versionNumber = data.version;
            document.getElementById('version-number').textContent = versionNumber;
        })
        .catch(error => console.error('Error fetching version:', error));
}

// Run the function when the page loads
window.onload = fetchVersionAndUpdate;
