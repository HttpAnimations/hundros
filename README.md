# Hundros
Hundros is a repo based torrent engine deisgend for whatever the user want's just pls respect copyright law.

# READ
Hundros is moving to a new repo for the webserver the main branch is too cloudterd to work on anymore make sure to use branch electron and not main.

## Installing - Build 
To install open a new terminal and run the following commands.

1) Git clone the main branch.

```bash
git clone -b main https://gitlab.com/HttpAnimations/Hundros.git
```

2) Enter the new dir.

```bash
cd Hundros
```

3) Install the needed packages.

```bash
npm install
```

4) Build for your system.

```bash
npm run make
```

If you are only gonna deploy on this system then your good hear you are free to install the package in the out dir.

5) Build for all systems Linux/macOS/NT.

```bash
npm run build
```

# Installing - Binary
This will go over installing from a bianry.

1) Go to [here](https://gitlab.com/HttpAnimations/Hundros/-/releases).

2) Download the bianry for your system am on **[Manjaro](manjaro.org/)** so am gonna use the basic bianary.

3) Extract if needed.

4) Install - Deb

```bash
dpkg -i file.deb
```

5) Install - Rpm

```bash
sudo dnf install file.rpm
```

6) Install - Basic Linux

Extract the zip then run **Hundros** from the extracted folder. 

7) macOS
Extract and put into /Applications



8) Install - NT

Run the .exe file


# Repos
Repos are the way of providing software and media.

## Adding repos
To add a repo open Hundros and go to Settings and then the type of repo you want to add now you can add any repo to anything as they all use the same base but note if you do that your torrents won't be orginzed for this we will add a movie repo so go to **Hundros/Settings/Movies/** you will see something like this screen.

![photo](images/Screenshot%202024-05-03%20at%2012.24.09.png)

Where it says *Enter a new repo URL* enter the url for your new repo for this we will use the [public domain repo for movies](https://gitlab.com/HttpAnimations/Hundros/-/tree/Public-Repos?ref_type=heads) this will be the deafult repo you have if you want to get more repos you can go [here](https://gitlab.com/HttpAnimations/Hundros/-/tree/Repos?ref_type=heads) witch has every known repo but note these might contain media that is copyrighted none of them are tested for the content only if it works the only ones that are tested are the offical ones but with aside paste in the url.

![photo](images/Screenshot%202024-05-03%20at%2012.29.32.png)

Now click on *Add Repo* to add your new repo you will need write premissions to edit the file.

![photo](images/Screenshot%202024-05-03%20at%2012.32.47.png)

## Removing repos
If you would like to remove a repo either becuase it's broken laggy or malware or just don't want it anymore you can do so by opening the settings page and then click on the thing you wanna move eg. Moveis, ~~porn~~ TV. To do this go to **Hundros/Settings/MEDIA** eg. moves **Hundros/Settings/Movies** now you will see a screen like this one.

![photo](images/Screenshot%202024-05-03%20at%2012.37.18.png)

Once here click on the remove button

![photo](images/Screenshot%202024-05-03%20at%2012.38.07.png)

![photo](images/Screenshot%202024-05-03%20at%2012.38.23.png)


## Making a new repo
If you would like to make a new repo you need the following

1) Basic **json** knowgle

2) IDE | [VSC](https://code.visualstudio.com/)

3) touch

4) :3

<br>

- Make a new folder.
```bash
mkdir ~/RepoEXP
```

- Open the folder.
```bash
cd ~/RepoEXP
```

- Open the folder in a IDE 
```bash
code .
```

- Make a new file called what ever you want for this we want movies repo to do make a new file called **Movies.json**

```
touch Movies.json
```

- Edit that file add the following **Json** code into it.

```json
{
    "Torrents": [
        {
            "Name": "NAMEOFTORRENT",
            ".Torrent": "DOWNLOADFORTORRENT",
            "MagnetUrl": "MagnetURL",
            "Icon": "URL",
            "SeedersAtTheTime": "9",
            "Publisher": "WHOMADETHETORRENTFILE",
            "HasStreamURL": false,
            "StreamURL": "IFYOUCANSTREAMTHESHOWTHENADDTHEURLHERE",
            "Source": "WHERETHETORRENTCAMEOVER"
        }
    ]
}
```

- **Name** | The name of the torrent this can not be left blank.

```json
"Name": "Example Torrent",
```

- **.Torrent** | The .torrent download you can leave the filed blank for no download

```json
".Torrent": "https://example.com/torrents/torrentname.torrent",
```

```json
".Torrent": "",
```

- **MagnetUrl** | The magnet URL of the torrent this can be left blank for nothing

```json
"MagnetUrl": "magnet:?xt=urn:btih:EXAMPLEMAGNETURL",
```

```json
"MagnetUrl": "",
```

- **Icon** | This is the icon of the torrent. **Note 1.1.1+**

```json
"Icon": "https://example.com/images/photo.png",
```

- **SeedersAtTheTime** | The amount of seeders at the time of making the torrent this does not have to be a number if the publisher has seeder eg. [Archive](https://archive.org)

```json
"SeedersAtTheTime": "69",
```

- **Publisher** | This is who made the torrent most of time you can use who made the real torrent from the info panel or the website you got it from but you can also your repos name.

```json
"Publisher": "Hundros",
```

- **HasStreamURL** | This is a ture or false statement false meaning the torrent is most likly not a video true meaning you can watch the video online this is usally linked up with a [Youtube](https://youtube.com) or [Archive](https://archive.org) video.

- **StreamURL** | This goes along with the **HasStreamURL** and is the URL to watch the video if you have **HasStreamURL** to false then you can just leave this blank if you have to true tho you will need to enter the URL to watch the thing.

```json
"StreamURL": "https://example.com/videos/video.mp4",
```

- **Source** | This is where the torrent came from this usally a [Archive](https://archive.org) URL.

```json
"Source": "https://gitlab.com/HttpAnimations/Hundros"
```

## Special repos
Some repos need some more info.

## Oculus Quest

1) **TestedOn**
This should be placed after **Publisher**
```json
"TestedOn": "Q1 - Q2 - Q3 - QPro",
```

# Keyboard shortcuts
**command/ctrl + shift + p** : Dev tools

# More indev

## How the version number is displayed
To get the version number we use a **js** script to read a file called **package.json** witch has the **"version"** value using that value we send the data to things that need it. Here is a expmale on how it used on the index page.

```js
        // Function to fetch and display the version number from package.json
        function fetchVersion() {
            fetch('package.json')
            .then(response => response.json())
            .then(data => {
                const versionDiv = document.getElementById('version');
                versionDiv.innerHTML = 'Version: ' + data.version; // Assuming there's a "version" key in your package.json
            })
            .catch(error => console.error('Error loading the version:', error));
        }
```

# Mods
Mods a are a way of loading in custom code into **Hundros**.

## Installing mods
To install mods open the **Hundros** folder where **Hundros** is installed if you are on [macOS](https://apple.com/macos) then right click on the **Hundros** app and click on ***Show Package Contents*** now open the folder **resources/app** witch houses the main source code of **Hundros** be careful not to delete anything here as you can easly break the app doing so open the folder **Mods** and drag the root folder for your mod in **MODS CAN NOT BE A IN ARCHIVE OR SUB FOLDER** now in the root of source code run the following script.

```bash
bash ModLoader.sh
```

## Devlopment
If you would like to make a mod make a new folder with the following files. NodeJS code should be working for mods if you need more dependisces tho you will need the user to recompile.

1) package.json

```json
{
    "name": "ModTestName",
    "version": "1.0.0",
    "info": "This is where you place info about your mod.",
    "main": "index.html",
    "dir": "Mods/Mod1",
    "author": "NAME" 
}
```
2) index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

Let's start off with the **package.json** script.

1) name
Name is the name of your mod.
```json
"name": "Moretorrents",
```

2) version
Version is what version your mod is one this can be anything but try to keep it numbers.
```json
"version": "0.0.1",
```

3) info
Info is just a little bit about your mod usally what it dose.
```json
"info": "Add more torrents that Hundros does not work with.",
```

4) main
Main is the main html file to load when going to the mod this should be kept as index.html but can be witch ever one like a intro. This will start from the root dir of your mod not Hundros.
```json
"main": "index.html",
```

5) dir
Dir is the path to mod Mod1 being the folder that your mod is in this starts from the root of the **Hundros** dir.
```json
"dir": "Mods/Mod1",
```

6) author
Author is who made the mod this will usally be a [Gitlab](https://gitlab.com) or a [Github](https://github.com) user.
```json
"author": "HttpAnimations" 
```

# History
The history on Hundros started in 2020 when I first found about priacy and I downloaded over **300gb** of moveis onto my pc but then I got banned by my ISP and then made a website called **FYC**, **FYC** first started on [Github](https://github.com) and was made for stealing content everything was hardcoded but after a while [Github](https://github.com) found the proeject and locked my account so it moved over onto a  [Discord](https://discord.com) server **FYC** was being worked n, on and off until a webite was made called Torrent-File-Maker witch would make a torrent based on the info you enter after that **FYC** was worked on stederly until the website was no longer fun so it stoped being worked on and that was the end of **FYC**. Until a few months later in 2023 a new spark come over to my mind and a new version of **FYC** was being made witch was also for stealing witch this one was the same as the old **FYC** then like the first version this one was ended a few months later. For my secend attampt I took a new brand new approsh at doing it I made it use ***json*** files from online witch would hold all the new data for torrents then a single page from the web beowswer would get all the data and then send it out to ***json*** script for every torrent. Witch became very very slow over time after adding jsut a few repos I ended up redoing the whole way of loading torrents witch was a lot more effient witch was still used as today.

# Repo guidelines
Repos are free to do whatever but any with known malware torrent's eg. **Steamunlocked** torrent's will be forbidden from becomeing the main branch priacy is also prohibbeted but **PR Repos** will be premitted into the known repos with some restrictions.

# Credits

[https://github.com/apancik/public-domain-icons](https://github.com/apancik/public-domain-icons)

[https://commons.wikimedia.org/wiki/File:Gear_icon-72a7cf.svg](https://commons.wikimedia.org/wiki/File:Gear_icon-72a7cf.svg)

[https://www.rawpixel.com/image/10192827/vector-vhs-tape-illustrations-public-domain](https://www.rawpixel.com/image/10192827/vector-vhs-tape-illustrations-public-domain)

[http://www.publicdomainfiles.com][http://www.publicdomainfiles.com]

[https://www.remove.bg](https://www.remove.bg)

[https://www.electronjs.org/docs/latest/](https://www.electronjs.org/docs/latest/)

# Distribution Guidelines for Hundros

If you wish to distribute Hundros, it's important to adhere to the official packaging guidelines. **Repackaging of any files, including but not limited to `.zip (Darwin), .rpm (Linux)`, `.deb (Linux)`, `.zip (Linux)` or `.exe (NT)` formats, is strictly prohibited**. You must utilize the packages provided by Hundros. If you need to package Hundros in a format not supported by the existing system, you are required to create a new project and compile the source code directly from the official repository, available here: [Hundros Official Repository](https://gitlab.com/HttpAnimations/Hundros/-/tree/Repos?ref_type=heads). If you are using **OnlyLoadAppFromAsar** and or **Asar** then no builds must be present and the user must compile from source if the source code is the same as the package then you are allowed to have a publish on the main branch.

For individual users who do not intend to distribute Hundros, you are permitted to compile from the main package. However, under no circumstances may you publish or redistribute the compiled files. This ensures compliance with Hundros distribution policies and respects the integrity of the software.