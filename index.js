// server.js
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const app = express();
const PORT = 9783;

app.use(cookieParser());
app.use(bodyParser.json());

// Custom middleware to filter out specific files
app.use((req, res, next) => {
  const blockedFiles = [
    'package.json',
    'package-lock.json',
    '.gitignore',
    'index.js',
    'preload.js'
  ];

  const filePath = path.join(__dirname, req.path);

  if (blockedFiles.includes(path.basename(filePath))) {
    res.status(403).send('Access to this file is forbidden');
  } else {
    next();
  }
});

// Serve all files except the blocked ones
app.get('*', (req, res) => {
  const filePath = path.join(__dirname, req.path);
  res.sendFile(filePath, (err) => {
    if (err) {
      res.status(404).send('File not found');
    }
  });
});

// Route to read a file
app.get('/read-file', (req, res) => {
  const filePath = path.join(__dirname, req.query.filePath);
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      res.status(500).send('Failed to read file');
    } else {
      res.send(data);
    }
  });
});

// Route to save a file
app.post('/save-file', (req, res) => {
  const filePath = path.join(__dirname, req.body.filePath);
  const content = req.body.content;
  fs.writeFile(filePath, content, 'utf8', (err) => {
    if (err) {
      res.status(500).send('Failed to save file');
    } else {
      res.send('File saved successfully');
    }
  });
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
