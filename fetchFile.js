function fetchFile() {
    const currentFileName = window.location.pathname.split('/').pop();
    const currentFileDiv = document.getElementById('currentFile');
    currentFileDiv.innerHTML = ' | Current file: ' + currentFileName;
}

// Call the function to display the current file name
fetchFile();