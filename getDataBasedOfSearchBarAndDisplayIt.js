document.addEventListener("DOMContentLoaded", function () {
    const searchBar = document.querySelector('.search-bar');
    const categoryLinks = document.querySelectorAll('.category-links a');

    // Function to filter category links based on search input
    function filterCategories(query) {
        const lowerCaseQuery = query.toLowerCase();

        // Loop over each category link
        categoryLinks.forEach(link => {
            // Check if the link's text includes the search query
            if (link.textContent.toLowerCase().includes(lowerCaseQuery)) {
                link.style.display = 'inline';  // Show matching link
            } else {
                link.style.display = 'none';  // Hide non-matching link
            }
        });

        // Optional: Display a message if no categories match
        updateNoMatchMessage();
    }

    // Function to update or display a "no match" message
    function updateNoMatchMessage() {
        let visibleLinks = Array.from(categoryLinks).filter(link => link.style.display !== 'none');
        const noMatchDiv = document.getElementById('no-match');
        if (visibleLinks.length === 0) {
            noMatchDiv.style.display = 'block';
        } else {
            noMatchDiv.style.display = 'none';
        }
    }

    // Add event listener for search input
    searchBar.addEventListener('input', function () {
        filterCategories(searchBar.value);
    });
});
