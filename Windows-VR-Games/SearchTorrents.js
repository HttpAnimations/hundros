async function searchTorrents() {
    const searchInput = document.getElementById('searchInput').value.toLowerCase();
    const contentArea = document.getElementById('contentArea');
    const allTorrentDivs = contentArea.getElementsByClassName('torrent');

    // First, hide all torrents
    Array.from(allTorrentDivs).forEach(div => {
        div.style.display = 'none';
    });

    // Filter and display matching torrents
    Array.from(allTorrentDivs).forEach(div => {
        const torrentName = div.getElementsByTagName('h4')[0].textContent.toLowerCase();
        if (torrentName.includes(searchInput)) {
            div.style.display = '';
        }
    });
}

document.getElementById('searchInput').addEventListener('keyup', function(event) {
    // Optional: Trigger search on pressing Enter key
    if (event.key === 'Enter') {
        searchTorrents();
    }
});


// This script is broken :( still working on fix