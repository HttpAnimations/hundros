fetch('https://raw.githubusercontent.com/HttpAnimation/RepoNEW/main/Repos.json')
    .then(response => response.json())
    .then(data => {
        const contentArea = document.getElementById('contentArea');
        data.Repos.categories.forEach(category => {
            const section = document.createElement('section');
            section.innerHTML = `<h2>${category.name}</h2>`;
            category.repos.forEach(repo => {
                const repoDiv = document.createElement('div');
                repoDiv.classList.add('torrent');

                // Displaying the repository name as plain text
                const repoName = document.createElement('span');
                repoName.textContent = repo.name;
                repoDiv.appendChild(repoName);

                // Two break lines for better separation
                repoDiv.appendChild(document.createElement('br'));
                repoDiv.appendChild(document.createElement('br'));

                // Code block for the URL
                const codeBlock = document.createElement('code');
                codeBlock.textContent = repo.url;
                repoDiv.appendChild(codeBlock);

                section.appendChild(repoDiv);
            });
            contentArea.appendChild(section);
        });
    });
